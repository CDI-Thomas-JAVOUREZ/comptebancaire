package com.bankaccount;

import java.util.Scanner;

import com.bankaccount.controllers.Account;

public class Main {
	
	public static void main (String[] args) {
		
		Account myAccount = null;
		
		Scanner sc = new Scanner(System.in);
		
		int saisie = 0;
		
		do {
						
			saisie = mainMenu(sc);
			
			switch(saisie) {
			
			case 1: {
				myAccount = new Account();
				break;
				
			}
				
			case 2: {
				System.out.print("Numero du compte :");
				String numeroCompte = sc.next();
				
				if(myAccount != null )
					System.out.println(myAccount.toString());
				else
					System.out.println("Le compte demandé n'existe pas. Veuillez saisir un numéro de compte existant.");
				break;
			}
				
			case 3: {
				System.out.println("Option non programm�e.");
				break;
			}
				
			case 4: {
				sc.close();
				exit();
				break;
			}
				
			case 5:
				help();
				break;
				
			default:
				System.out.println("Option non reconnue. Veuillez saisir une commande existante.");
				
			}
			
		} while(saisie != 4);
		
	}
	
	public static int mainMenu(Scanner sc) {

		System.out.println("************************************");
		System.out.println("* Application de gestion de compte *");
		System.out.println("************************************");
		System.out.println("* 1. Cr�er un compte               *");
		System.out.println("* 2. Afficher un compte            *");
		System.out.println("* 3. Cr�er une ligne comptable     *");
		System.out.println("* 4. Sortir                        *");
		System.out.println("* 5. De l'aide                     *");
		System.out.println("************************************");
		System.out.println("Votre choix : ");
		
		return sc.nextInt();
		
	}
	
	public static void exit() {
		System.out.println("Bonne journ�e !");
		System.exit(0);
	}
	
	public static void help() {
		System.out.println("Aide");
		System.out.println("1 -> Cr�er un nouveau compte : Saisir le type de compte, le num�ro de compte, le montant du d�pot, et le taux de placement si il s'agit d'un compte r�mun�r�.");
		System.out.println("2 -> Afficher un compte : Saisir le num�ro du compte qu'il faut afficher.");
		System.out.println("3 -> Cr�er une ligne comptable : Option non programm�e");
		System.out.println("4 -> Sortir du programme");
		System.out.println("5 -> Aide : Afficher l'aide");
	}

}
